import Taskerify from 'taskerify';
const uglify = require('gulp-uglify-es').default;

Taskerify.config.sourcemaps = false;
Taskerify.config.srcPath = './src';          // Src Path
Taskerify.config.distPath = './dist/assets'; // Dist Path
Taskerify.config.srcViewsPath = './views';        // Views Src Path
Taskerify.config.distViewsPath = './dist';   // Compiled Views Dist Path (HTML)

const SRC = Taskerify.config.srcPath;
const DIST = Taskerify.config.distPath;

const ARCHIVES = './dist/arquivos';
const FILES = './dist/files';

const storeName = 'greenpeace';
const commomFiles = ['queimadas'];

console.log(Taskerify.Plugins);
Taskerify((mix) => {
    // Common Files
    commomFiles.map((file) =>
        mix.browserify(`${SRC}/common/js/${storeName}-common-${file}.js`, ARCHIVES)
            .sass(`${SRC}/common/scss/${storeName}-common-${file}.scss`, ARCHIVES));

    // Main Desktop Files
    // desktopFiles.map((file) =>
    //     mix.browserify(`${SRC}/desktop/js/${storeName}-desktop-${file}.js`, ARCHIVES, { plugins: [uglify] })
    //         .sass(`${SRC}/desktop/scss/${storeName}-desktop-${file}.scss`, ARCHIVES));

    // Main Mobile Files
    // mobileFiles.map((file) =>
    //     mix.browserify(`${SRC}/mobile/js/${storeName}-mobile-${file}.js`, ARCHIVES, { plugins: [uglify] })
    //         .sass(`${SRC}/mobile/scss/${storeName}-mobile-${file}.scss`, ARCHIVES));

    // Checkout
    // mix.browserify(`${SRC}/common/js/checkout6-custom.js`, FILES)
    // mix.sass(`${SRC}/common/scss/checkout6-custom.scss`, FILES);

    // PugJS Template
    mix.pug();

    mix.vtex('greenpeace');
});
